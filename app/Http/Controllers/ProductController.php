<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use App\Entity\Product;

class ProductController extends Controller
{
    private $repository;
    private $redirectTo = RouteServiceProvider::HOME;

    public function __construct(Product $product)
    {
        $this->repository = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->repository->paginate(6);

        return view('product.list', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = $this->repository->newInstance();

        if (Gate::denies('create', $product)) {
            Session::flash('warning_msg', 'You don\'t have rights to create products!');
            return redirect($this->redirectTo);
        }

        $url = route('products.store');

        return view('product.create', compact('product', 'url'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        if (Gate::denies('create', $this->repository)) {
            Session::flash('warning_msg', 'You don\'t have rights to create products!');
            return redirect($this->redirectTo);
        }

        $product = $this->repository->newInstance();
        $product->user_id = Auth::id();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $result = $product->save();

        if ($result) {
            Session::flash('success_msg', "Product {$product->name} has been successfully created!");
        } else {
            Session::flash('error_msg', 'Some error happens try again later!');
        }

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->repository->find($id);

        if (Gate::denies('view', $product)) {
            Session::flash('warning_msg', 'You don\'t have rights to edit this product!');
            return redirect($this->redirectTo);
        }

        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->repository->find($id);

        if (Gate::denies('update', $product)) {
            Session::flash('warning_msg', 'You don\'t have rights to edit this product!');
            return redirect($this->redirectTo);
        }

        $url = route('products.update', $product->id);

        return view('product.edit', compact('product', 'url'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = $this->repository->find($id);

        if (Gate::denies('update', $product)) {
            Session::flash('warning_msg', 'You don\'t have rights to edit this product!');
            return redirect($this->redirectTo);
        }

        $product->name = $request->input('name');
        $product->price = $request->input('price');

        $result = $product->save();
        if ($result) {
            Session::flash('success_msg', "Product {$product->name} has been updated!");
        } else {
            Session::flash('error_msg', 'Some error happens try again later!');
        }

        return redirect()->route('products.view', ['id' => $product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->repository->find($id);

        if (Gate::denies('delete', $product)) {
            Session::flash('warning_msg', 'You don\'t have rights to delete this product!');
            return redirect($this->redirectTo);
        }

        $result = $product->delete();

        if ($result) {
            Session::flash('success_msg', "Product {$product->name} successfully deleted!");
        } else {
            Session::flash('error_msg', 'Some error happens try again later!');
        }

        return redirect()->route('products.index');
    }
}