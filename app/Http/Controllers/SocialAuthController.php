<?php

namespace App\Http\Controllers;

use App\Entity\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;

class SocialAuthController extends Controller
{
    protected $redirectPath = RouteServiceProvider::HOME;

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {

            $socialUser = Socialite::driver($provider)->user();

            $user = User::where(['email' => $socialUser->email])->first();

            if (!$user) {
                $user = User::create([
                    'name' => $socialUser->name,
                    'email' => $socialUser->email,
                    'password' => Hash::make('password'), //Just for demo password
                ]);
            }

            Auth::login($user);

            return redirect($this->redirectPath);
        } catch (\Exception $e) {
            Session::flash('error_msg', $e->getMessage());
            return redirect('/');
        }
    }
}
