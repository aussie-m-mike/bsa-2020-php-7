@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <h2>Edit product {{ $product->name }}</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4">
                @include('product._form')
            </div>
        </div>
    </div>
@endsection