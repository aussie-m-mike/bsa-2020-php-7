<form action="{{ $url }}" method="post">
    @csrf
    @if($product->exists)
        {{ method_field('PUT') }}
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name', $product->name)}}">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input type="text" class="form-control" id="price" name="price" value="{{old('price', $product->price)}}">
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>