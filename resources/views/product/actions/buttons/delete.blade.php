@can('delete', $product)
    <form
            action="{{ route('products.destroy', $product->id) }}"
            method="POST"
            style="display: inline-block"
    >
        @csrf
        {{ method_field('DELETE') }}
        <button class="btn btn-danger">Delete</button>
    </form>
@endcan