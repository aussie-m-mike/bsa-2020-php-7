@can('view', $product)
    <a href="{{route('products.show', $product->id)}}" class="btn btn-success">View</a>
@endcan