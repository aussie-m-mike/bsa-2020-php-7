@can('create', App\Entity\Product::class)
    <a href="{{route('products.create')}}" class="btn btn-success">Add new product</a>
@endcan