@can('update', $product)
    <a href="{{route('products.edit', $product->id)}}" class="btn btn-info">Edit</a>
@endcan