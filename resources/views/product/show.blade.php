@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">$ {{ $product->price }}</h6>
                        @include('product.actions.buttons.edit')
                        @include('product.actions.buttons.delete')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection