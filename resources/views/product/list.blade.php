@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if ($products)
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th scope="row">
                                    <a href="{{route('products.show', $product->id)}}">
                                        {{ $product->name }}
                                    </a>
                                </th>
                                <td>$ {{ $product->price }}</td>
                                <th class="text-right">
                                    @include('product.actions.buttons.view')
                                    @include('product.actions.buttons.edit')
                                    @include('product.actions.buttons.delete')
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <?php echo $products->render(); ?>
                @else
                    <div class="card">
                        <div class="card-body">
                            There are no products!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection