@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="bd-title" id="content">Marketplace</h1>
                <h6>Powered by <span>Laravel</span></h6>
            </div>
        </div>
    </div>
@endsection