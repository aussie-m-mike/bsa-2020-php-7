<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('marketplace');
});

Auth::routes();

Route::get('/auth/{name}', 'SocialAuthController@redirectToProvider');
Route::get('/auth/{name}/callback', 'SocialAuthController@handleProviderCallback');

Route::middleware([\App\Http\Middleware\Authenticate::class])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('products', 'ProductController');
});

